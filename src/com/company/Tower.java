package com.company;

import java.util.Stack;

public class Tower {
    private String name;
    private Stack<Integer> stack;

    public Tower(String name, Stack<Integer> stack) {
        this.name = name;
        this.stack = stack;
    }

    public String getName() {
        return name;
    }

    public Stack<Integer> getStack() {
        return stack;
    }
}
