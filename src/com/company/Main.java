package com.company;

import java.util.Scanner;
import java.util.Stack;

public class Main {

    public Tower towerA, towerB, towerC;

    public static void main(String[] args) {
        Tower towerA = new Tower("A",new Stack<>());
        Tower towerB = new Tower("B",new Stack<>());
        Tower towerC = new Tower("C",new Stack<>());
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter blocks number:");
        int numberOfBlocks = scanner.nextInt();
        scanner.nextLine();
        if (numberOfBlocks <= 0 || numberOfBlocks >=11){
            System.out.println("Number must be between 1 and 10.");
            System.exit(0);
        }
        for (int i = numberOfBlocks; i > 0; i--) {
            towerA.getStack().add(i);
        }
        int[] result = {numberOfBlocks,0,0};
        System.out.println("Pradinė būsena " + towerA.getName()+towerA.getStack().toString()+
                ", " + towerB.getName() + towerB.getStack().toString()+", "+towerC.getName()+towerC.getStack().toString());
        new HanoiTower(result,0).doHanoiTowerGame(numberOfBlocks,towerA,towerB,towerC);
    }
}
