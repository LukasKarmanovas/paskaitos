package com.company;

public class HanoiTower {
    int [] result;
    int counter;
    Tower A;
    Tower B; 
    Tower C;

    public HanoiTower(int[] result,int counter) {

        this.result = result;
        this.counter = counter;
    }

    public void doHanoiTowerGame(int towerBlocks, Tower from, Tower inter, Tower to){
        if (towerBlocks == 1){
            counter++;
            int tmp = from.getStack().pop();
            to.getStack().push(tmp);
            
            if (from.getName().equals("A")){
                A=from;
            }else if (inter.getName().equals("A")){
                A=inter;
            }else if (to.getName().equals("A")){
                A=to;
            }

            if (from.getName().equals("B")){
                B=from;
            }else if (inter.getName().equals("B")){
                B=inter;
            }else if (to.getName().equals("B")){
                B=to;
            }

            if (from.getName().equals("C")){
                C=from;
            }else if (inter.getName().equals("C")){
                C=inter;
            }else if (to.getName().equals("C")){
                C=to;
            }

            System.out.println(counter + ". Diską " +tmp+ " kelti nuo " + from.getName()+" ant " + to.getName() + ". " + A.getName()+A.getStack().toString()+
            ", " + B.getName() + B.getStack().toString()+", "+C.getName()+C.getStack().toString());
        }else {
            doHanoiTowerGame(towerBlocks - 1, from, to, inter);
            counter++;
            int tmp = from.getStack().pop();
            to.getStack().push(tmp);


            if (from.getName().equals("A")){
                A=from;
            }else if (inter.getName().equals("A")){
                A=inter;
            }else if (to.getName().equals("A")){
                A=to;
            }

            if (from.getName().equals("B")){
                B=from;
            }else if (inter.getName().equals("B")){
                B=inter;
            }else if (to.getName().equals("B")){
                B=to;
            }

            if (from.getName().equals("C")){
                C=from;
            }else if (inter.getName().equals("C")){
                C=inter;
            }else if (to.getName().equals("C")){
                C=to;
            }


            System.out.println(counter + ". Diską " +tmp+ " kelti nuo " + from.getName()+" ant " + to.getName() + ". " + A.getName()+A.getStack().toString()+
                    ", " + B.getName() + B.getStack().toString()+", "+C.getName()+C.getStack().toString());
            doHanoiTowerGame(towerBlocks - 1, inter, from, to);
        }
    }
}
